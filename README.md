## Installation

- Pull le repository
- Lancer un swarm: ``docker swarm init``

## Configuration

- Modifier le mot de passe dans le fichier .env

## Lancer l'infra

- Utiliser la commande `` docker stack deploy --compose-file docker-compose.yml swarm ``
- Lancer l'installation de Wordpress (via le navigateur) puis installer l'extension Serveur info.
- Pour scaler les conteneurs wordpress, utiliser la commande suivante: ``docker service scale swarm_wordpress=nombre_de_conteneurs``
Pour vérifier le fonctionnement du load balancing aller dans le Dashboard du Wordpress. Le hostname est unique pour chaque conteneur, ainsi quand on recharge la page on accède aux différents conteneurs.